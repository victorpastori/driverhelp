package com.example.victor.driverhelp.Activities;

import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.victor.driverhelp.Model.Sistema;
import com.example.victor.driverhelp.R;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;

public class DadosSistemaActivity extends BaseActivity {
    // InputLayouts
    private TextInputLayout inputLayoutPrecoCombustivel;
    private TextInputLayout inputLayoutConsumoMedio;
    /*private TextInputLayout inputLayoutValorRevisao;
    private TextInputLayout inputLayoutPerRevisao;
    private TextInputLayout inputLayoutValorPneus;
    private TextInputLayout inputLayoutPerPneus;*/
    // InputEditTexts
    private TextInputEditText edtPrecoCombustivel;
    private TextInputEditText edtConsumoMedio;
    /*private TextInputEditText edtValorRevisao;
    private TextInputEditText edtPerRevisao;
    private TextInputEditText edtValorPneus;
    private TextInputEditText edtPerPneus;*/
    private Button btnSalvar;
    // Stored
    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_sistema);
        // Create Toolbar
        inflateToolbar();
        getSupportActionBar().setTitle(R.string.configuracao);
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8073048307141645~5649689144");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // Get Preferences
        preferences = getSharedPreferences(Sistema.DADOS_SISTEMA, 0);
        // Get Views Layout
        findViews();
        // Create masks
        SimpleMaskFormatter smfPC = new SimpleMaskFormatter("N.NN");
        MaskTextWatcher mtwData = new MaskTextWatcher(edtPrecoCombustivel, smfPC);
        edtPrecoCombustivel.addTextChangedListener(mtwData);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFormularioValido()){
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putFloat(Sistema.CONSUMO_MEDIO, Float.parseFloat(String.valueOf(edtConsumoMedio.getText())));
                    editor.putFloat(Sistema.PRECO_COMBUSTIVEL, Float.parseFloat(String.valueOf(edtPrecoCombustivel.getText())));
                    //editor.putFloat(Sistema.VALOR_PNEUS, Float.parseFloat(String.valueOf(edtValorPneus.getText())));
                    //editor.putFloat(Sistema.PER_PNEUS, Float.parseFloat(String.valueOf(edtPerPneus.getText())));
                    //editor.putFloat(Sistema.VALOR_REVISAO, Float.parseFloat(String.valueOf(edtValorRevisao.getText())));
                    //editor.putFloat(Sistema.PER_REVISAO, Float.parseFloat(String.valueOf(edtPerRevisao.getText())));
                    // Calculando media de custo
                    editor.putFloat(Sistema.CUSTO_COMBUSTIVEL_KM,
                            Float.parseFloat(String.valueOf(edtPrecoCombustivel.getText()))/Float.parseFloat(String.valueOf(edtConsumoMedio.getText())));
                    //editor.putFloat(Sistema.CUSTO_PNEU_KM,
                            //Float.parseFloat(String.valueOf(edtValorPneus.getText()))/Float.parseFloat(String.valueOf(edtPerPneus.getText())));
                    //editor.putFloat(Sistema.CUSTO_REVISAO_KM,
                            //Float.parseFloat(String.valueOf(edtValorRevisao.getText()))/Float.parseFloat(String.valueOf(edtPerRevisao.getText())));
                    editor.commit();
                    atualizaDadosAcitivity();
                    finish();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        atualizaDadosAcitivity();
    }

    private void validateForm(){

    }

    protected void atualizaDadosAcitivity(){
        if (preferences.contains(Sistema.CONSUMO_MEDIO)){
            edtConsumoMedio.setText((String.valueOf(preferences.getFloat(Sistema.CONSUMO_MEDIO, 0))));
        }
        if (preferences.contains(Sistema.PRECO_COMBUSTIVEL)){
            edtPrecoCombustivel.setText((String.valueOf(preferences.getFloat(Sistema.PRECO_COMBUSTIVEL, 0))));
        }
        /*if (preferences.contains(Sistema.PER_REVISAO)){
            edtPerRevisao.setText((String.valueOf(preferences.getFloat(Sistema.PER_REVISAO, 0))));
        }
        if (preferences.contains(Sistema.VALOR_REVISAO)){
            edtValorRevisao.setText((String.valueOf(preferences.getFloat(Sistema.VALOR_REVISAO, 0))));
        }
        if (preferences.contains(Sistema.PER_PNEUS)){
            edtPerPneus.setText((String.valueOf(preferences.getFloat(Sistema.PER_PNEUS, 0))));
        }
        if (preferences.contains(Sistema.VALOR_PNEUS)){
            edtValorPneus.setText((String.valueOf(preferences.getFloat(Sistema.VALOR_PNEUS, 0))));
        }*/
    }

    private boolean isFormularioValido(){
        boolean ret = true;
        if(edtPrecoCombustivel.getText().toString().isEmpty()){
            inputLayoutPrecoCombustivel.setErrorEnabled(true);
            inputLayoutPrecoCombustivel.setError("*Obrigatório");
            ret = false;
        }
        if(edtConsumoMedio.getText().toString().isEmpty()){
            inputLayoutConsumoMedio.setErrorEnabled(true);
            inputLayoutConsumoMedio.setError("*Obrigatório");
            ret = false;
        }
        /*if(edtValorRevisao.getText().toString().isEmpty()){
            inputLayoutValorRevisao.setErrorEnabled(true);
            inputLayoutValorRevisao.setError("*Obrigatório");
            ret = false;
        }
        if(edtPerRevisao.getText().toString().isEmpty()){
            inputLayoutPerRevisao.setErrorEnabled(true);
            inputLayoutPerRevisao.setError("*Obrigatório");
            ret = false;
        }
        if(edtValorPneus.getText().toString().isEmpty()){
            inputLayoutValorPneus.setErrorEnabled(true);
            inputLayoutValorPneus.setError("*Obrigatório");
            ret = false;
        }
        if(edtPerPneus.getText().toString().isEmpty()){
            inputLayoutPerPneus.setErrorEnabled(true);
            inputLayoutPerPneus.setError("*Obrigatório");
            ret = false;
        }*/
        return ret;
    }

    private void findViews(){
        // Get InputLayouts of View
        inputLayoutConsumoMedio = findViewById(R.id.textInputLayoutKmRodados);
        inputLayoutPrecoCombustivel = findViewById(R.id.textInputLayoutValorRecebido);
        /*inputLayoutPerPneus = findViewById(R.id.textInputLayoutPeriodicidadeTrocaPneu);
        inputLayoutPerRevisao = findViewById(R.id.textInputLayoutData);
        inputLayoutValorPneus = findViewById(R.id.textInputLayoutValorTrocaPneu);
        inputLayoutValorRevisao = findViewById(R.id.textInputLayoutHoras);*/
        // Get InputEditTexts of View
        edtPrecoCombustivel = findViewById(R.id.inputPrecoCombustivel);
        edtConsumoMedio = findViewById(R.id.inputConsumoMedio);
        /*edtValorRevisao =  findViewById(R.id.inputValorRevisao);
        edtPerRevisao =  findViewById(R.id.inputPeriodicidadeRevisao);
        edtValorPneus =  findViewById(R.id.inputValorTrocaPneu);
        edtPerPneus =  findViewById(R.id.inputPeriodicidadeTrocaPneu);*/
        // Get Button of View
        btnSalvar =  findViewById(R.id.btnSalvar);
    }
}
