package com.example.victor.driverhelp.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

//@Entity(foreignKeys = @ForeignKey(entity = Demonstrativo.class,
  //      parentColumns = "id",
    //    childColumns = "demonstrativo_id"))
@Entity(indices = {@Index(value = {"data"})})
public class Despesa implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    @NonNull
    private double valor;
    private String categoria;
    private String descricao;
    @NonNull
    private String data;
    private boolean auto;
    private int mes;
    private int ano;

   // @ColumnInfo(name = "demonstrativo_id")
    //private int demonstrativoId;

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    @NonNull
    public double getValor() {
        return valor;
    }

    public void setValor(@NonNull double valor) {
        this.valor = valor;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @NonNull
    public String getData() {
        return data;
    }

    public void setData(@NonNull String data) {
        this.data = data;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    /*@NonNull
    public int getDemonstrativoId() {
        return demonstrativoId;
    }

    public void setDemonstrativoId(@NonNull int demonstrativoId) {
        this.demonstrativoId = demonstrativoId;
    }*/
}
