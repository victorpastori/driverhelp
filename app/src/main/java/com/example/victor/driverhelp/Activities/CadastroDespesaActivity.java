package com.example.victor.driverhelp.Activities;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;

import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.Model.DespesaRepository;
import com.example.victor.driverhelp.R;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CadastroDespesaActivity extends BaseActivity {
    private AdView mAdView;
    public static final String EXTRA_REPLY = "com.example.android.demonstrativolist.REPLY";
    private AppCompatSpinner spinnerCategoria;
    private TextInputLayout inputLayoutValor;
    private TextInputLayout inputLayoutDescricao;
    private TextInputLayout inputLayoutData;
    private TextInputEditText edtValor;
    private TextInputEditText edtDescricao;
    private TextInputEditText edtData;
    private Calendar myCalendar;
    private Button btnSalvar;
    private final String[] categorias = new String[] {"Combustível", "Manutenção", "Documentação", "Limpeza", "Alimentação", "Água/Balas"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myCalendar = Calendar.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_despesa);
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8073048307141645~5649689144");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Create toolbar
        inflateToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Nova despesa");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDespesa)));
        // Load views
        findViews();
        // Populate spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, categorias);
        spinnerCategoria.setAdapter(spinnerAdapter);
        // Mask Data
        SimpleMaskFormatter smfData = new SimpleMaskFormatter("NN/NN/NNNN");
        MaskTextWatcher mtwData = new MaskTextWatcher(edtData, smfData);
        edtData.addTextChangedListener(mtwData);

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        edtData.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    new DatePickerDialog(CadastroDespesaActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });
        edtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CadastroDespesaActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()){
                    Despesa despesa = new Despesa();
                    despesa.setData(edtData.getText().toString());
                    despesa.setValor(Double.parseDouble(edtValor.getText().toString()));
                    String[] data = despesa.getData().split("/");
                    despesa.setMes(Integer.parseInt(data[1]));
                    despesa.setAno(Integer.parseInt(data[2]));
                    despesa.setAuto(false);
                    despesa.setDescricao(edtDescricao.getText().toString());
                    despesa.setCategoria(spinnerCategoria.getSelectedItem().toString());
                    DespesaRepository repository = new DespesaRepository(getApplication());
                    repository.insert(despesa);
                    Intent replyIntent = new Intent();
                    replyIntent.putExtra(EXTRA_REPLY, despesa);
                    setResult(RESULT_OK, replyIntent);
                    finish();
                }
            }
        });
    }

    private void findViews(){
        // Spinner
        spinnerCategoria = findViewById(R.id.appCompatSpinner);
        // Layouts input
        inputLayoutValor = findViewById(R.id.textInputLayoutValor);
        inputLayoutDescricao = findViewById(R.id.textInputLayoutDescricao);
        inputLayoutData = findViewById(R.id.textInputLayoutData);
        // Edit texts
        edtValor = findViewById(R.id.inputValor);
        edtDescricao = findViewById(R.id.inputDescricao);
        edtData = findViewById(R.id.inputData);
        //Button
        btnSalvar = findViewById(R.id.btnSalvarDespesa);

    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, new Locale("pt","BR"));
        edtData.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean validateForm(){
        boolean ret = true;
        if(edtValor.getText().toString().isEmpty()){
            inputLayoutValor.setErrorEnabled(true);
            inputLayoutValor.setError("*Obrigatório");
            ret = false;
        }
        if(edtData.getText().toString().isEmpty()){
            inputLayoutData.setErrorEnabled(true);
            inputLayoutData.setError("*Obrigatório");
            ret = false;
        }
        return ret;
    }

}
