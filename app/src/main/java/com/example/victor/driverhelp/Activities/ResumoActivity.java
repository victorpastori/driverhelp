package com.example.victor.driverhelp.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.Model.DespesaSum;
import com.example.victor.driverhelp.Model.ResumoViewModel;
import com.example.victor.driverhelp.R;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ResumoActivity extends BaseActivity implements OnChartValueSelectedListener {
    private CardView clickCardViewGanhosDiarios;
    private CardView clickCardViewDetalhesDespesa;
    private AppCompatTextView txtFaturamento;
    private AppCompatTextView txtDespeas;
    private PieChart pieChart;
    private PieChart halfPieChart;
    private HorizontalBarChart horizontalBarChart;
    private ResumoViewModel resumoViewModel;
    private AppCompatSpinner spinnerMes;
    private final String[] meses = new String[] {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
    private Calendar calendar;
    private NumberFormat currrencyFormat;
    private MaterialCalendarView calendarView;
    private CalendarDay mCalendarDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumo);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8073048307141645~5649689144");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        currrencyFormat = NumberFormat.getCurrencyInstance();

        clickCardViewDetalhesDespesa = findViewById(R.id.cardViewDetalhesDespesas);
        clickCardViewGanhosDiarios = findViewById(R.id.cardViewGanhosDiarios);
        txtFaturamento = findViewById(R.id.txtFaturamento);
        txtDespeas = findViewById(R.id.txtDespesas);

        clickCardViewGanhosDiarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ResumoActivity.this, MainActivity.class);
                intent.putExtra("mes", mCalendarDay.getMonth()+1);
                intent.putExtra("ano", mCalendarDay.getYear());
                startActivity(intent);
            }
        });

        clickCardViewDetalhesDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ResumoActivity.this, DespesasActivity.class);
                intent.putExtra("mes", mCalendarDay.getMonth()+1);
                intent.putExtra("ano", mCalendarDay.getYear());
                startActivity(intent);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        halfPieChart = findViewById(R.id.halfPieChart);
        halfPieChart.setUsePercentValues(true);
        halfPieChart.getDescription().setEnabled(false);
        halfPieChart.setExtraOffsets(0,10,0,0);
        halfPieChart.setRotationEnabled(false);

        halfPieChart.setDragDecelerationFrictionCoef(0.95f);

        halfPieChart.setDrawHoleEnabled(true);
        halfPieChart.setHoleColor(Color.WHITE);

        halfPieChart.setDrawCenterText(true);

        //halfPieChart.setTransparentCircleRadius(61f);
        halfPieChart.setOnChartValueSelectedListener(this);

        Demonstrativo demonstrativo = new Demonstrativo(0,0);
        setDataHalfPie(demonstrativo);


        pieChart = findViewById(R.id.pieChartDespesas);
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(0,10,0,0);
        pieChart.setRotationEnabled(false);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setDrawCenterText(true);

        pieChart.setOnChartValueSelectedListener(this);
        List<DespesaSum> d = new ArrayList<>();
        setDataPie(d);

        resumoViewModel = ViewModelProviders.of(this).get(ResumoViewModel.class);
        resumoViewModel.getDespesas().observe(this, new Observer<List<DespesaSum>>() {
            @Override
            public void onChanged(@Nullable List<DespesaSum> despesas) {
                if(despesas.size() == 0){
                    pieChart.setCenterText(generateCenterSpannableText("Sem dados no período"));
                }else{
                    pieChart.setCenterText(generateCenterSpannableText("Despesas"));
                }
                setDataPie(despesas);
            }
        });

        resumoViewModel.getDemonstrativo().observe(this, new Observer<Demonstrativo>() {
            @Override
            public void onChanged(@Nullable Demonstrativo demonstrativo) {
                if(demonstrativo == null){
                    demonstrativo = new Demonstrativo(0,0);
                    halfPieChart.setCenterText(generateCenterSpannableText("Sem dados no período"));
                }else{
                    halfPieChart.setCenterText(generateCenterSpannableText("Lucro: "+currrencyFormat.format(demonstrativo.getValorBruto()-demonstrativo.getDespesa())));
                }
                setDataHalfPie(demonstrativo);
            }
        });
        calendarView = findViewById(R.id.calendarView);
        calendarView.setTitleMonths(meses);
        calendar = Calendar.getInstance();
        mCalendarDay = calendarView.getCurrentDate();
        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                resumoViewModel.setAno(calendarDay.getYear());
                resumoViewModel.setMes(calendarDay.getMonth()+1);
                mCalendarDay = calendarDay;
            }
        });

        com.github.clans.fab.FloatingActionButton fabDespesa = findViewById(R.id.fabDespesa);
        fabDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ResumoActivity.this, CadastroDespesaActivity.class);
                startActivityForResult(i, NEW_DESPESA);
            }
        });

        com.github.clans.fab.FloatingActionButton fabGanho = findViewById(R.id.fabGanho);
        fabGanho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ResumoActivity.this, CadastroDemonstrativoActivity.class);
                startActivityForResult(i, NEW_GANHO);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        resumoViewModel.setAno(calendar.get(Calendar.YEAR));
        resumoViewModel.setMes(calendar.get(Calendar.MONTH)+1);
    }

    private void setDataHalfPie(Demonstrativo demonstrativo) {

        ArrayList<PieEntry> values = new ArrayList<PieEntry>();
        double despesa = demonstrativo.getDespesa();
        double valor = demonstrativo.getValorBruto();
        values.add(new PieEntry((float) (valor-despesa), "Lucro"));
        values.add(new PieEntry((float) (despesa), "Despesa"));

        PieDataSet dataSet = new PieDataSet(values, "Lucro/Despesa");
        dataSet.setSliceSpace(1f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(new int[]{Color.rgb(00,211,158), Color.rgb(255,112,106)});
        //dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        halfPieChart.setData(data);
        halfPieChart.invalidate();

        txtFaturamento.setText(currrencyFormat.format(valor));
        //txtDespeas.setText(String.valueOf(despesa));
    }

    public void setDataPie(List<DespesaSum> despesas){
        ArrayList<PieEntry> pieValues = new ArrayList<>();
        double despesa = 0;
        for (int i = 0; i < despesas.size() ; i++){
            despesa += despesas.get(i).getTotal();
            pieValues.add(new PieEntry((float) despesas.get(i).getTotal(),despesas.get(i).getCategoria()));
        }
        PieDataSet dataSet = new PieDataSet(pieValues, "Despesas");
        dataSet.setSliceSpace(1f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.YELLOW);
        pieChart.setData(data);
        pieChart.invalidate();
        txtDespeas.setText(currrencyFormat.format(despesa));
    }

    private SpannableString generateCenterSpannableText(String content) {

        SpannableString s = new SpannableString(content);
        return s;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
