package com.example.victor.driverhelp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.R;

import java.text.NumberFormat;
import java.util.List;

public class DespesaAdapter extends RecyclerView.Adapter<DespesaAdapter.DespesaViewHolder> {

    class DespesaViewHolder extends RecyclerView.ViewHolder{
        private final TextView txtValor;
        private final TextView textTipo;
        public DespesaViewHolder(View itemView) {
            super(itemView);
            this.txtValor = itemView.findViewById(R.id.txtValor);
            this.textTipo = itemView.findViewById(R.id.txtTipo);
        }
    }

    private NumberFormat currencyFormat;
    private final LayoutInflater layoutInflater;
    private List<Despesa> despesas;

    public List<Despesa> getDespesas() {
        return despesas;
    }

    public void setDespesas(List<Despesa> despesas) {
        this.despesas = despesas;
        notifyDataSetChanged();
    }


    public DespesaAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        currencyFormat = NumberFormat.getCurrencyInstance();
    }



    @NonNull
    @Override
    public DespesaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.despesa_layout_item, parent, false);
        return new DespesaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DespesaViewHolder holder, int position) {
        Despesa current = despesas.get(position);
        holder.textTipo.setText("("+current.getCategoria()+")");
        holder.txtValor.setText(currencyFormat.format(current.getValor()));
    }

    @Override
    public int getItemCount() {
        if(despesas!= null){
            return despesas.size();
        }else{
            return 0;
        }
    }
}
