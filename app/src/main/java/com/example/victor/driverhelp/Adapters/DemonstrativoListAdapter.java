package com.example.victor.driverhelp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.R;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.List;

public class DemonstrativoListAdapter extends RecyclerView.Adapter<DemonstrativoListAdapter.DemonstrativoViewHolder> {
    private NumberFormat currencyFormat;

    class DemonstrativoViewHolder extends RecyclerView.ViewHolder{
        private final TextView ganhos;
        private final TextView kmRodados;
        private final TextView data;
        private final TextView horasTrabalhadas;

        public DemonstrativoViewHolder(View itemView) {
            super(itemView);
            ganhos = itemView.findViewById(R.id.txtValorLiquido);
            kmRodados = itemView.findViewById(R.id.txtKmRodados);
            data = itemView.findViewById((R.id.txtData));
            horasTrabalhadas = itemView.findViewById(R.id.txtHoras);

        }
    }

    private final LayoutInflater layoutInflater;
    private List<Demonstrativo> demonstrativos;

    public DemonstrativoListAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
        currencyFormat = NumberFormat.getCurrencyInstance();
    }

    public void setDemonstrativos(List<Demonstrativo> demonstrativos){
        this.demonstrativos = demonstrativos;
        notifyDataSetChanged();
    }

    public List<Demonstrativo> getDemonstrativos(){
        return this.demonstrativos;
    }

    @NonNull
    @Override
    public DemonstrativoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.demonstrativo_layout_itemv2, parent, false);
        return new DemonstrativoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DemonstrativoViewHolder holder, int position) {
        Demonstrativo current = demonstrativos.get(position);
        double valor = current.getValorBruto();
        double despesa = current.getDespesa();
        holder.ganhos.setText(currencyFormat.format(valor));
        holder.kmRodados.setText(String.valueOf(current.getKmRodados()) + "km");
        //holder.valorLiquido.setText("R$ " + String.format("%.2f", (valor-despesa)));
        holder.data.setText(current.getData());
        holder.horasTrabalhadas.setText(String.valueOf(current.getHorasTrabalhadas())+"h:"+String.valueOf(current.getMinutosTrabalhados())+"m");
    }

    @Override
    public int getItemCount() {
        if (demonstrativos!=null){
            return demonstrativos.size();
        }else{
            return 0;
        }
    }


}
