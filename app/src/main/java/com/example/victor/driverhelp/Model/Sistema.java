package com.example.victor.driverhelp.Model;

public class Sistema {
    public static final String DADOS_SISTEMA = "DadosSistema";
    public static final String CONSUMO_MEDIO = "ConsumoMedio";
    public static final String VALOR_REVISAO = "ValorRevisao";
    public static final String PER_REVISAO = "PeriodicidadeRevisao";
    public static final String VALOR_PNEUS = "valorPneus";
    public static final String PER_PNEUS = "PeridiocidadeTrocaPneus";
    public static final String PRECO_COMBUSTIVEL = "PrecoCombustivel";
    public static final String CUSTO_COMBUSTIVEL_KM = "CCKM";
    public static final String CUSTO_REVISAO_KM = "CRKM";
    public static final String CUSTO_PNEU_KM = "CPKM";
    public static final String CALCULO_AUTOMATICO = "CalculoAutomatico";

    private double consumoMedio;
    private double valorRevisao;
    private double periodicidadeRevisao;
    private double valorPneus;
    private double peridiocidadeTrocaPneus;
    private double precoCombustivel;
    private boolean calculoAutomatico;

    public Sistema(double consumoMedio, double valorRevisao, double periodicidadeRevisao, double valorPneus,
                   double peridiocidadeTrocaPneus, double precoCombustivel, boolean calculoAutomatico) {
        this.consumoMedio = consumoMedio;
        this.valorRevisao = valorRevisao;
        this.periodicidadeRevisao = periodicidadeRevisao;
        this.valorPneus = valorPneus;
        this.peridiocidadeTrocaPneus = peridiocidadeTrocaPneus;
        this.precoCombustivel = precoCombustivel;
        this.calculoAutomatico = calculoAutomatico;
    }

    public double getConsumoMedio() {
        return consumoMedio;
    }

    public double getValorRevisao() {
        return valorRevisao;
    }

    public double getPeriodicidadeRevisao() {
        return periodicidadeRevisao;
    }

    public double getValorPneus() {
        return valorPneus;
    }

    public double getPeridiocidadeTrocaPneus() {
        return peridiocidadeTrocaPneus;
    }

    public double getPrecoCombustivel() {
        return precoCombustivel;
    }

    public boolean isCalculoAutomatico() {
        return calculoAutomatico;
    }
}
