package com.example.victor.driverhelp.Model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import java.util.List;

public class DemonstrativoDetalheViewModel extends AndroidViewModel {
    //Repositorios
    private DespesaRepository mRepository;
    private DemonstrativoRepository mDemRepository;

    private MutableLiveData<Integer> mDemonstrativoId = new MutableLiveData<>();
    private MutableLiveData<String> dataDemonstrativo = new MutableLiveData<>();
    private LiveData<Demonstrativo> mDemonstrativo;

    private LiveData<List<Despesa>> mDespesasDemonstrativo;

    public DemonstrativoDetalheViewModel(Application application) {
        super(application);
        mRepository = new DespesaRepository(application);
        mDemRepository = new DemonstrativoRepository(application);

        //mDespesasDemonstrativo = Transformations.switchMap(mDemonstrativoId, mRepository::getDespesasDemonstrativo);
        mDespesasDemonstrativo = Transformations.switchMap(dataDemonstrativo, data -> mRepository.getmDespesaData(data));

        mDemonstrativo = Transformations.switchMap(mDemonstrativoId, id ->{
            return mDemRepository.getDemonstrativo(id);
        });
    }

    public LiveData<List<Despesa>>  getDespesasDemonstrativo(){
        return mDespesasDemonstrativo;
    }

    public LiveData<Demonstrativo> getDemonstrativo() {
        return mDemonstrativo;
    }

    public void insert (Despesa despesa){
        mRepository.insert(despesa);
    }

    public void setDemonstrativoId(int demonstrativoId){
        mDemonstrativoId.setValue(demonstrativoId);
    }

    public void setDataDemonstrativo(String data){
        dataDemonstrativo.setValue(data);
    }
}
