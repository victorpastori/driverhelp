package com.example.victor.driverhelp.Activities;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.example.victor.driverhelp.Adapters.DemonstrativoListAdapter;
import com.example.victor.driverhelp.Adapters.RecyclerItemClickListener;
import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.Model.DemonstrativoViewModel;
import com.example.victor.driverhelp.Model.Sistema;
import com.example.victor.driverhelp.R;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

public class MainActivity extends BaseActivity {
    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;
    public static final int CONFIGURATION_ACTIVITY_REQUEST_CODE = 2;
    private final String[] meses = new String[] {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
    SharedPreferences preferences;
    private DemonstrativoViewModel mDemViewModel;
    private AppCompatSpinner spinnerMes;
    private Calendar calendar;
    private AppCompatTextView txtLucro;
    private NumberFormat currrencyFormat;
    private MaterialCalendarView calendarView;
    private int mes;
    private int ano;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8073048307141645~5649689144");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        currrencyFormat = NumberFormat.getCurrencyInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ganhos");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryGanho)));

        txtLucro = findViewById(R.id.txtLucro);

        //spinnerMes = findViewById(R.id.appCompatSpinner);
        //ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, meses);
        //spinnerMes.setAdapter(spinnerAdapter);
        //calendar = Calendar.getInstance();
        //spinnerMes.setSelection(calendar.get(Calendar.MONTH));

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final DemonstrativoListAdapter adapter = new DemonstrativoListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDemViewModel = ViewModelProviders.of(this).get(DemonstrativoViewModel.class);
        mDemViewModel.getDemonstrativos().observe(this, new Observer<List<Demonstrativo>>() {
            @Override
            public void onChanged(@Nullable List<Demonstrativo> demonstrativos) {
                adapter.setDemonstrativos(demonstrativos);
            }
        });

        mDemViewModel.getDemonstrativo().observe(this, new Observer<Demonstrativo>() {
            @Override
            public void onChanged(@Nullable Demonstrativo demonstrativo) {
                if(demonstrativo == null){
                    txtLucro.setText(currrencyFormat.format(0));
                }else{
                    txtLucro.setText(currrencyFormat.format(demonstrativo.getValorBruto() - demonstrativo.getDespesa()));
                }
            }
        });
        calendarView = findViewById(R.id.calendarView);
        calendarView.setTitleMonths(meses);
        calendar = Calendar.getInstance();
        mes = intent.getIntExtra("mes",calendar.get(Calendar.MONTH)+1);
        ano = intent.getIntExtra("ano", calendar.get(Calendar.YEAR));
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                mDemViewModel.setAno(calendarDay.getYear());
                mDemViewModel.setMes(calendarDay.getMonth()+1);
            }
        });

        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                mDemViewModel.setAno(calendarDay.getYear());
                mDemViewModel.setMes(calendarDay.getMonth()+1);
            }
        });

        /*spinnerMes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                mDemViewModel.setMes(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Demonstrativo demonstrativo = adapter.getDemonstrativos().get(position);
                                Intent i = new Intent(MainActivity.this, DemonstrativoActivity.class);
                                i.putExtra("demonstrativo", demonstrativo);
                                startActivity(i);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                createDialog(position, adapter);
                            }

                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            }
                        }
                )
        );
        com.github.clans.fab.FloatingActionButton fabDespesa = findViewById(R.id.fabDespesa);
        fabDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, CadastroDespesaActivity.class);
                startActivityForResult(i, NEW_DESPESA);
            }
        });

        com.github.clans.fab.FloatingActionButton fabGanho = findViewById(R.id.fabGanho);
        fabGanho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, CadastroDemonstrativoActivity.class);
                startActivityForResult(i, NEW_GANHO);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        CalendarDay date = CalendarDay.from(ano, mes-1, 1);
        calendarView.setCurrentDate(date);
        mDemViewModel.setAno(ano);
        mDemViewModel.setMes(mes);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.preferences = getSharedPreferences(Sistema.DADOS_SISTEMA, 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            //Demonstrativo demonstrativo = (Demonstrativo) data.getExtras().getSerializable(CadastroDemonstrativoActivity.EXTRA_REPLY);
            //mDemViewModel.preferences = this.preferences;
            //mDemViewModel.insert(demonstrativo);
        } else {
        }
    }

    private void createDialog(int position, DemonstrativoListAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Excluir ganho");
        builder.setMessage("Caso esse ganho tenha gerado despesas automáticas elas também serão excluídas.");
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Demonstrativo demonstrativo = adapter.getDemonstrativos().get(position);
                mDemViewModel.delete(demonstrativo);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }
}
