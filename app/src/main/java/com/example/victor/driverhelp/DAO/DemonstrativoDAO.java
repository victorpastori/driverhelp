package com.example.victor.driverhelp.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.Model.Despesa;

import java.util.List;

@Dao
public interface DemonstrativoDAO {

    @Insert
    long insert(Demonstrativo demonstrativo);

    @Update
    int update(Demonstrativo demonstrativo);

    @Query("DELETE from Demonstrativo")
    void deleteAll();

    @Query("DELETE from Demonstrativo where id = :id")
    void delete(int id);

    @Query("SELECT *, (select sum(valor) from despesa s where s.data = d.data group by s.data) as despesa from Demonstrativo d where mes = :mes " +
            "and ano = :ano order by data DESC")
    //@Query("SELECT * from Demonstrativo where mes = :mes and ano = :ano order by data DESC")
    LiveData<List<Demonstrativo>> getDemonstrativos(int mes, int ano);

    //@Query("SELECT SUM(valorBruto) as valorBruto, SUM(valorLiquido) as valorLiquido, SUM(kmRodados) as kmRodados from Demonstrativo where mes = :mes and ano = :ano")
    @Query("SELECT id, sum(valorBruto) as valorBruto, sum(kmRodados) as kmRodados, " +
            "sum(horasTrabalhadas) as horasTrabalhadas, sum(minutosTrabalhados) as minutosTrabalhados, " +
            "data, mes, ano, (select sum(valor) from despesa s where s.mes = :mes and s.ano = :ano) as despesa from Demonstrativo d where mes = :mes and ano = :ano")
    LiveData<Demonstrativo> getSumDemonstrativo(int mes, int ano);

    @Query("SELECT *, (select sum(valor) from despesa s where s.data = d.data) as despesa  from Demonstrativo d where id = :demonstrativoId")
    LiveData<Demonstrativo> getDemonstrativo(int demonstrativoId);

    @Query("SELECT *, (select sum(valor) from despesa s where s.data = d.data) as despesa  from Demonstrativo d where data = :data")
    Demonstrativo getDemonstrativo(String data);
}
