package com.example.victor.driverhelp.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.R;
import com.google.android.gms.ads.AdView;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    public AdView mAdView;
    public static int CONFIGURATION_ACTIVITY_REQUEST_CODE = 2;
    public static final int NEW_DESPESA = 10;
    public static final int NEW_GANHO = 11;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                Intent i = new Intent(this, DadosSistemaActivity.class);
                startActivityForResult(i, CONFIGURATION_ACTIVITY_REQUEST_CODE);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }

    public void inflateToolbar(){
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitle(R.string.app_name);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_resumo) {
            Intent intent = new Intent(this, ResumoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_demonstrativos) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_despesas) {
            Intent intent = new Intent(this, DespesasActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_novo_demonstrativo) {
            Intent intent = new Intent(this, CadastroDemonstrativoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_nova_despesa) {
            Intent intent = new Intent(this, CadastroDespesaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_config) {
            Intent intent = new Intent(this, DadosSistemaActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
