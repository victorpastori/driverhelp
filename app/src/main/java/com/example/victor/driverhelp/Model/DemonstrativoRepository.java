package com.example.victor.driverhelp.Model;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.example.victor.driverhelp.DAO.DemonstrativoDAO;
import com.example.victor.driverhelp.DAO.DespesaDAO;
import com.example.victor.driverhelp.DAO.DriverHelpDatabase;

import java.text.DecimalFormat;
import java.util.List;

public class DemonstrativoRepository implements AsyncResponse{
    private DemonstrativoDAO mDemonstrativoDao;
    private DespesaDAO mDespesaDao;
    private LiveData<List<Demonstrativo>> mDemonstrativos;
    public static SharedPreferences preferences;
    private LiveData<Demonstrativo> mDemonstrativo;

    public DemonstrativoRepository(Application application){
        DriverHelpDatabase db = DriverHelpDatabase.getDatabase(application);
        mDemonstrativoDao = db.demonstrativoDAO();
        mDespesaDao = db.despesaDAO();
        //mDemonstrativos = mDemonstrativoDao.getAllDemonstrativos();
    }

    LiveData<List<Demonstrativo>> getAllDemonstrativos(){
        return mDemonstrativos;
    }

    LiveData<List<Demonstrativo>> getDemonstrativos(int mes, int ano){
        return mDemonstrativoDao.getDemonstrativos(mes, ano);
    }

    LiveData<Demonstrativo> getDemonstrativo(int id){
        return mDemonstrativoDao.getDemonstrativo(id);
    }

    LiveData<Demonstrativo> getSumMes(int mes, int ano){
        return mDemonstrativoDao.getSumDemonstrativo(mes, ano);
    }

    public void insert (Demonstrativo demonstrativo){
        insertAsyncTask iat = new insertAsyncTask(mDemonstrativoDao, mDespesaDao);
        iat.delegate = this;
        iat.execute(demonstrativo);
    }

    public void delete(Demonstrativo demonstrativo){
        deleteAsyncTask dat = new deleteAsyncTask(mDemonstrativoDao, mDespesaDao);
        dat.execute(demonstrativo);
    }

    public long updateValorLiquido(int demId, double despesa){

        return 1;
    }

    public Demonstrativo existeDemonstrativo(String data){
        return mDemonstrativoDao.getDemonstrativo(data);
    }

    private static class deleteAsyncTask extends AsyncTask<Demonstrativo, Void, Void> {
        private DemonstrativoDAO mAsyncTaskDao;
        private DespesaDAO mAsyncTaskDespesaDao;

        deleteAsyncTask(DemonstrativoDAO dao, DespesaDAO dDao){
            mAsyncTaskDao = dao;
            mAsyncTaskDespesaDao = dDao;
        }
        @Override
        protected Void doInBackground(Demonstrativo... demonstrativos) {
            mAsyncTaskDao.delete(demonstrativos[0].getId());
            mAsyncTaskDespesaDao.deleteDespesasAuto(demonstrativos[0].getData());
            return null;
        }
    }

    private static class insertAsyncTask extends AsyncTask<Demonstrativo, Void, Long> {

        private DemonstrativoDAO mAsyncTaskDao;
        private DespesaDAO mAsyncTaskDespesaDao;
        public AsyncResponse delegate = null;

        insertAsyncTask(DemonstrativoDAO dao, DespesaDAO dDao){
            mAsyncTaskDao = dao;
            mAsyncTaskDespesaDao = dDao;
        }

        @Override
        protected Long doInBackground(Demonstrativo... demonstrativos) {
            long demId = 0;
            Demonstrativo dem = mAsyncTaskDao.getDemonstrativo(demonstrativos[0].getData());
            if(dem != null){
                demonstrativos[0].setId(dem.getId());
                if(mAsyncTaskDao.update(demonstrativos[0]) > 0){
                    mAsyncTaskDespesaDao.deleteDespesasAuto(demonstrativos[0].getData());
                    demId = dem.getId();
                }
            }else{
                demId = mAsyncTaskDao.insert(demonstrativos[0]);
            }
            if (demonstrativos[0].isCalculoAutomatico()){
                // Manutencao Revisão
                /*Despesa manutencao = new Despesa();
                manutencao.setData(demonstrativos[0].getData());
                manutencao.setCategoria("Manutenção");
                manutencao.setDescricao("Revisão(Automático)");
                manutencao.setMes(demonstrativos[0].getMes());
                manutencao.setAno(demonstrativos[0].getAno());
                manutencao.setAuto(true);
                // Manutencao Pneus
                Despesa manutencao2 = new Despesa();
                manutencao2.setData(demonstrativos[0].getData());
                manutencao2.setCategoria("Manutenção");
                manutencao2.setDescricao("Pneus(Automático)");
                manutencao2.setMes(demonstrativos[0].getMes());
                manutencao2.setAno(demonstrativos[0].getAno());
                manutencao2.setAuto(true);*/
                // Combustivel
                Despesa combustivel = new Despesa();
                combustivel.setData(demonstrativos[0].getData());
                combustivel.setCategoria("Combustível");
                combustivel.setDescricao("(Automático)");
                combustivel.setMes(demonstrativos[0].getMes());
                combustivel.setAno(demonstrativos[0].getAno());
                combustivel.setAuto(true);
                // Formato decimal
                DecimalFormat formato = new DecimalFormat(".##");
                // Calculo valores das despesas
                double custoCombustivel = demonstrativos[0].getDespesaGasolina(preferences.getFloat(Sistema.CUSTO_COMBUSTIVEL_KM,0));
                custoCombustivel = Double.parseDouble(formato.format(custoCombustivel).replace(",","."));
                combustivel.setValor(custoCombustivel);
                /*double custoManutencao = demonstrativos[0].getDespesaManutencao(preferences.getFloat(Sistema.CUSTO_REVISAO_KM,0));
                custoManutencao = Double.valueOf(formato.format(custoManutencao).replace(",", "."));
                manutencao.setValor(custoManutencao);
                double custoManutencao2 = demonstrativos[0].getDespesaManutencao(preferences.getFloat(Sistema.CUSTO_PNEU_KM,0));
                custoManutencao2 = Double.valueOf(formato.format(custoManutencao2).replace(",", "."));
                manutencao2.setValor(custoManutencao2);*/
                // Inserindo
                mAsyncTaskDespesaDao.insert(combustivel);
                //mAsyncTaskDespesaDao.insert(manutencao);
                //mAsyncTaskDespesaDao.insert(manutencao2);
            }
            return demId;
        }

        @Override
        protected void onPostExecute(Long aVoid) {
            delegate.processFinish(aVoid.longValue());
        }
    }

    @Override
    public void processFinish(long id) {

    }
 }
