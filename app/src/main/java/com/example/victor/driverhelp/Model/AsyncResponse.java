package com.example.victor.driverhelp.Model;

public interface AsyncResponse {
    void processFinish(long id);
}
