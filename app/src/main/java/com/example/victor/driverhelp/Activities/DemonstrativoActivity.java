package com.example.victor.driverhelp.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.victor.driverhelp.Adapters.DespesaAdapter;
import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.Model.DemonstrativoDetalheViewModel;
import com.example.victor.driverhelp.Model.DemonstrativoViewModel;
import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.List;

public class DemonstrativoActivity extends BaseActivity {
    private AdView mAdView;
    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;
    SharedPreferences preferences;
    private DemonstrativoViewModel mDemonstrativoViewModel;
    private DemonstrativoDetalheViewModel mDespesaViewModel;

    private TextView txtValorLiquido;
    private TextView txtValorBruto;
    private TextView txtDespesas;
    private TextView txtDataDemonstrativo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demonstrativo);
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8073048307141645~5649689144");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Criar toolbar
        inflateToolbar();
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Extrato");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryGanho)));


        txtDespesas = findViewById(R.id.txtDespesas);
        txtValorBruto = findViewById(R.id.txtValorBruto);
        txtValorLiquido = findViewById(R.id.txtValorLiquido);
        txtDataDemonstrativo = findViewById(R.id.txtDataDemonstrativo);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewDespesas);
        final DespesaAdapter adapter = new DespesaAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        mDemonstrativoViewModel = ViewModelProviders.of(this).get(DemonstrativoViewModel.class);
        mDespesaViewModel = ViewModelProviders.of(this).get(DemonstrativoDetalheViewModel.class);
        mDespesaViewModel.setDemonstrativoId(((Demonstrativo) getIntent().getSerializableExtra("demonstrativo")).getId());
        mDespesaViewModel.setDataDemonstrativo(((Demonstrativo) getIntent().getSerializableExtra("demonstrativo")).getData());
        mDespesaViewModel.getDespesasDemonstrativo().observe(this, new Observer<List<Despesa>>() {
            @Override
            public void onChanged(@Nullable List<Despesa> despesas) {
                adapter.setDespesas(despesas);
            }
        });

        mDespesaViewModel.getDemonstrativo().observe(this, new Observer<Demonstrativo>() {
            @Override
            public void onChanged(@Nullable Demonstrativo demonstrativo) {

                txtValorLiquido.setText("R$" + String.format("%.2f", demonstrativo.getValorBruto()-demonstrativo.getDespesa()));
                txtValorBruto.setText("R$" + String.format("%.2f", demonstrativo.getValorBruto()));
                txtDespesas.setText("R$" + String.format("%.2f", demonstrativo.getDespesa()));
                txtDataDemonstrativo.setText("Extrato do dia: "+ demonstrativo.getData());
                getSupportActionBar().setTitle("Extrato dia "+demonstrativo.getData());
            }
        });

        FloatingActionButton fab = findViewById(R.id.fabDemDet);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DemonstrativoActivity.this, CadastroDespesaActivity.class);
                startActivityForResult(i, NEW_WORD_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Despesa despesa = (Despesa) data.getExtras().getSerializable(CadastroDemonstrativoActivity.EXTRA_REPLY);
            mDespesaViewModel.insert(despesa);
        } else {
        }
    }
}
