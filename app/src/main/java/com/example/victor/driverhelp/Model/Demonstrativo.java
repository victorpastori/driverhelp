package com.example.victor.driverhelp.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(indices = {@Index(value = {"data"}, unique = true)})
public class Demonstrativo  implements Serializable{
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    @NonNull
    private double valorBruto;
    private int horasTrabalhadas;
    private int minutosTrabalhados;
    private double kmRodados;
    private String data;
    private int mes;
    private int ano;
    @Ignore
    private boolean calculoAutomatico;
    @Ignore
    private boolean update;
    private double despesa;

    public double getDespesa() {
        return despesa;
    }

    public void setDespesa(double despesa) {
        this.despesa = despesa;
    }

    public  Demonstrativo(){

    }

    public Demonstrativo(double valorBruto, double despesa){
        this.valorBruto = valorBruto;
        this.despesa = despesa;
    }
    public Demonstrativo(double valorBruto, int horasTrabalhadas, int minutosTrabalhados,
                         double kmRodados, String data) {
        this.valorBruto = valorBruto;
        this.horasTrabalhadas = horasTrabalhadas;
        this.minutosTrabalhados = minutosTrabalhados;
        this.kmRodados = kmRodados;
        this.data = data;
    }

    public void update(Demonstrativo demonstrativo){
        this.id = demonstrativo.getId();
        this.valorBruto += demonstrativo.getValorBruto();
        this.kmRodados += demonstrativo.getKmRodados();
        this.minutosTrabalhados += demonstrativo.getMinutosTrabalhados();
        this.horasTrabalhadas += demonstrativo.getHorasTrabalhadas();
        if(this.minutosTrabalhados >= 60){
            this.minutosTrabalhados -= 60;
            this.horasTrabalhadas++;
        }
    }

    public double calcularDespesa(float combustivelKm, float revisaoKm, float pneuKm){
        return kmRodados*(combustivelKm+revisaoKm+pneuKm);
    }

    public double getDespesaGasolina(float custo){
        return custo*kmRodados;
    }

    public double getDespesaManutencao(float custo){
        return custo*kmRodados;
    }

    // GETS e SETS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setValorBruto(double valorBruto) {
        this.valorBruto = valorBruto;
    }

    public void setHorasTrabalhadas(int horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }

    public void setMinutosTrabalhados(int minutosTrabalhados) {
        this.minutosTrabalhados = minutosTrabalhados;
    }

    public void setKmRodados(double kmRodados) {
        this.kmRodados = kmRodados;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValorBruto() {
        return valorBruto;
    }

    public int getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public int getMinutosTrabalhados() {
        return minutosTrabalhados;
    }

    public double getKmRodados() {
        return kmRodados;
    }

    public String getData() {
        return data;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public boolean isCalculoAutomatico() {
        return calculoAutomatico;
    }

    public void setCalculoAutomatico(boolean calculoAutomatico) {
        this.calculoAutomatico = calculoAutomatico;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }
}
