package com.example.victor.driverhelp.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.Model.DespesaSum;

import java.util.List;

@Dao
public abstract class DespesaDAO {

    @Insert
    public abstract void insert(Despesa despesa);

    @Query("DELETE from Despesa")
    public abstract void deleteAll();

    @Query("DELETE from Despesa where id = :id")
    public abstract void delete(int id);

    @Query("SELECT id, SUM(valor) as valor, descricao, categoria, auto, data, mes, ano from Despesa where data = :data group by categoria order by categoria ASC")
    public abstract LiveData<List<Despesa>> getDespesasByData(String data);

    //@Query("SELECT SUM(valor) as valor, descricao from Despesa where mes = :mes and ano = :ano group by descricao")
    //@Query("SELECT id, sum(valor) as valor, descricao, demonstrativo_id, data, mes, ano from Despesa where mes = :mes and ano = :ano group by descricao order by descricao")
    @Query("SELECT sum(valor) as total, descricao, categoria, data from Despesa where mes = :mes and ano = :ano group by categoria order by categoria")
    public abstract LiveData<List<DespesaSum>> getSumDespesaMes(int mes, int ano);

    @Query("SELECT * from Despesa where mes = :mes and ano = :ano order by data DESC")
    public abstract LiveData<List<Despesa>> getDespesasMes(int mes, int ano);

    @Query("SELECT sum(valor) as total from Despesa where mes = :mes and ano = :ano")
    public abstract LiveData<Double> getTotalDespesaMes(int mes, int ano);

    @Query("DELETE from despesa where data = :data and auto = 1")
    public abstract int deleteDespesasAuto(String data);

    @Transaction
    public void insertDespesaAndUpdateDemonstrativo(Despesa despesa){
        insert(despesa);
        //updateDemonstrativo(despesa.getValor(), despesa.getDemonstrativoId());
    }
}
