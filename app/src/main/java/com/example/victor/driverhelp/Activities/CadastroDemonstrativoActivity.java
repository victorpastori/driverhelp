package com.example.victor.driverhelp.Activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.arch.lifecycle.LiveData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.victor.driverhelp.DAO.DemonstrativoDAO;
import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.Model.DemonstrativoRepository;
import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.Model.Sistema;
import com.example.victor.driverhelp.R;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.example.victor.driverhelp.Activities.MainActivity.CONFIGURATION_ACTIVITY_REQUEST_CODE;

public class CadastroDemonstrativoActivity extends BaseActivity {
    private AdView mAdView;
    public static final String CALCULO_AUTOMATICO = "calculoAutomatico";
    public static final String EXTRA_REPLY = "com.example.android.demonstrativolist.REPLY";
    private Calendar myCalendar;
    private TextInputLayout inputLayoutValorBruto;
    private TextInputLayout inputLayoutHorasTrabalhadas;
    private TextInputLayout inputLayoutKmRodados;
    private TextInputLayout inputLayoutData;
    private TextInputEditText edtValorBruto;
    private TextInputEditText edtHorasTrabalhadas;
    private TextInputEditText edtMinutosTrabalhados;
    private TextInputEditText edtKmRodados;
    private TextInputEditText edtData;
    private AlertDialog dialog;
    SharedPreferences pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myCalendar = Calendar.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_demonstrativo);
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8073048307141645~5649689144");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        inflateToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Novo ganho");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryGanho)));
        // Get Views Layout
        findViews();
        // Mask Data
        SimpleMaskFormatter smfData = new SimpleMaskFormatter("NN/NN/NNNN");
        MaskTextWatcher mtwData = new MaskTextWatcher(edtData, smfData);
        edtData.addTextChangedListener(mtwData);
        // Mask Horas Trabalhadas
        SimpleMaskFormatter smfHoras = new SimpleMaskFormatter("NN:NN");
        MaskTextWatcher mtwHoras = new MaskTextWatcher(edtHorasTrabalhadas, smfHoras);
        edtHorasTrabalhadas.addTextChangedListener(mtwHoras);

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        edtData.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    new DatePickerDialog(CadastroDemonstrativoActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });
        edtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CadastroDemonstrativoActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Button btnSalvar = (Button) findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean itsOk = true;
                CheckBox cb = findViewById(R.id.checkBoxCalculoAutomatico);
                if(validateForm()){
                    String[] horas = edtHorasTrabalhadas.getText().toString().split(":");
                    Demonstrativo demonstrativo = new Demonstrativo(
                            Double.parseDouble(edtValorBruto.getText().toString()),
                            Integer.parseInt(horas[0]),
                            Integer.parseInt(horas[1]),
                            Double.parseDouble(edtKmRodados.getText().toString()),
                            edtData.getText().toString()
                    );
                    String[] data = demonstrativo.getData().split("/");
                    demonstrativo.setMes(Integer.parseInt(data[1]));
                    demonstrativo.setAno(Integer.parseInt(data[2]));
                    demonstrativo.setCalculoAutomatico(false);
                    if(cb.isChecked()){
                        if(checkDadosSistema()){
                            demonstrativo.setCalculoAutomatico(true);
                        }else{
                            // Exibir alerta ao usuário para preencher dados ou desmarcar a opção
                            itsOk = false;
                            createDialog();
                        }
                    }
                    if(itsOk){
                        DemonstrativoRepository repository = new DemonstrativoRepository(getApplication());
                        repository.preferences = getSharedPreferences(Sistema.DADOS_SISTEMA, 0);
                        repository.insert(demonstrativo);
                        //demonstrativo.setUpdate(false);
                        Intent replyIntent = new Intent();
                        replyIntent.putExtra(EXTRA_REPLY, demonstrativo);
                        setResult(RESULT_OK, replyIntent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.pref = getSharedPreferences(Sistema.DADOS_SISTEMA, 0);
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, new Locale("pt","BR"));
        edtData.setText(sdf.format(myCalendar.getTime()));
    }

    /*private boolean checkDadosSistema(){
        if (!pref.contains(Sistema.CONSUMO_MEDIO) || !pref.contains(Sistema.PRECO_COMBUSTIVEL)
                || !pref.contains(Sistema.PER_REVISAO) || !pref.contains(Sistema.VALOR_REVISAO)
                || !pref.contains(Sistema.VALOR_PNEUS) || !pref.contains(Sistema.PER_PNEUS)){
            return false;
        }
        return true;
    }*/

    private boolean checkDadosSistema(){
        if (!pref.contains(Sistema.CONSUMO_MEDIO) || !pref.contains(Sistema.PRECO_COMBUSTIVEL)){
            return false;
        }
        return true;
    }

    private boolean validateForm(){
        boolean ret = true;
        if(edtValorBruto.getText().toString().isEmpty()){
            inputLayoutValorBruto.setErrorEnabled(true);
            inputLayoutValorBruto.setError("*Obrigatório");
            ret = false;
        }
        if(edtKmRodados.getText().toString().isEmpty()){
            inputLayoutKmRodados.setErrorEnabled(true);
            inputLayoutKmRodados.setError("*Obrigatório");
            ret = false;
        }
        if(edtData.getText().toString().isEmpty()){
            inputLayoutData.setErrorEnabled(true);
            inputLayoutData.setError("*Obrigatório");
            ret = false;
        }
        if(edtHorasTrabalhadas.getText().toString().isEmpty()){
            inputLayoutHorasTrabalhadas.setErrorEnabled(true);
            inputLayoutHorasTrabalhadas.setError("*Obrigatório");
            ret = false;
        }
        return ret;
    }

    private void findViews(){
        // Layouts input
        inputLayoutValorBruto = findViewById(R.id.textInputLayoutValorRecebido);
        inputLayoutHorasTrabalhadas = findViewById(R.id.textInputLayoutHoras);
        inputLayoutKmRodados = findViewById(R.id.textInputLayoutKmRodados);
        inputLayoutData = findViewById(R.id.textInputLayoutData);
        // Edit texts
        edtValorBruto = findViewById(R.id.inputValorRecebido);
        edtHorasTrabalhadas = findViewById(R.id.inputHoras);
        edtMinutosTrabalhados = findViewById(R.id.inputHoras); // ajustar isso posteriormente
        edtKmRodados = findViewById(R.id.inputKmRodados);
        edtData = findViewById(R.id.inputData);
    }

    private void createDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Atenção");
        builder.setMessage("Você precisa configurar os valores para ativar o cálculo automático, ou desmarcar essa opção.");
        builder.setPositiveButton("Configurar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(CadastroDemonstrativoActivity.this, DadosSistemaActivity.class);
                startActivityForResult(intent, CONFIGURATION_ACTIVITY_REQUEST_CODE);
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void createDialogAlertUpdate(Demonstrativo demonstrativo){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Atenção");
        builder.setMessage("Já existe faturamento cadastrado para esta data. Ao prosseguir o faturamento será substituido" +
                "pelo novo registro. ");
        builder.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent replyIntent = new Intent();
                replyIntent.putExtra(EXTRA_REPLY, demonstrativo);
                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
        builder.setNeutralButton("Voltar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private boolean existeDemonstrativo(String data) {
        DemonstrativoRepository demonstrativoRepository = new DemonstrativoRepository(getApplication());
        Demonstrativo demonstrativo = demonstrativoRepository.existeDemonstrativo(data);
        if (demonstrativo != null) {
            return true;
        }
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
