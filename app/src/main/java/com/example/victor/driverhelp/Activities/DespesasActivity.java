package com.example.victor.driverhelp.Activities;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;

import com.example.victor.driverhelp.Adapters.DemonstrativoListAdapter;
import com.example.victor.driverhelp.Adapters.DespesaListAdapter;
import com.example.victor.driverhelp.Adapters.RecyclerItemClickListener;
import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.Model.DespesasViewModel;
import com.example.victor.driverhelp.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;

public class DespesasActivity extends BaseActivity {
    private DespesasViewModel viewModel;
    private AppCompatTextView txtTotalDespesa;
    private Calendar calendar;
    private MaterialCalendarView calendarView;
    private NumberFormat currencyFormat;
    private int mes;
    private int ano;
    private AlertDialog dialog;
    private final String[] meses = new String[] {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_despesas);
        Intent intent = getIntent();

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-8073048307141645~5649689144");
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Despesas");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDespesa)));

        currencyFormat = NumberFormat.getCurrencyInstance();

        txtTotalDespesa = findViewById(R.id.txtTotalDespesas);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewDespesasList);
        final DespesaListAdapter adapter = new DespesaListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        viewModel = ViewModelProviders.of(this).get(DespesasViewModel.class);
        viewModel.getDespesas().observe(this, new Observer<List<Despesa>>() {
            @Override
            public void onChanged(@Nullable List<Despesa> despesas) {
                adapter.setDespesas(despesas);
            }
        });

        viewModel.getDespesaTotal().observe(this, new Observer<Double>() {
            @Override
            public void onChanged(@Nullable Double d) {
                if(d ==null){
                    txtTotalDespesa.setText(currencyFormat.format(0));
                }else{
                    double total = d.doubleValue();
                    txtTotalDespesa.setText(currencyFormat.format(total));
                }
            }
        });

        calendarView = findViewById(R.id.calendarView);
        calendarView.setTitleMonths(meses);
        calendar = Calendar.getInstance();
        mes = intent.getIntExtra("mes",calendar.get(Calendar.MONTH)+1);
        ano = intent.getIntExtra("ano", calendar.get(Calendar.YEAR));
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                viewModel.setAno(calendarDay.getYear());
                viewModel.setMes(calendarDay.getMonth()+1);
            }
        });
        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
                viewModel.setAno(calendarDay.getYear());
                viewModel.setMes(calendarDay.getMonth()+1);
            }
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                createDialog(position, adapter);
                            }

                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            }
                        }
                ));

        CalendarDay date = CalendarDay.from(ano, mes-1, 1);
        calendarView.setCurrentDate(date);
        viewModel.setAno(ano);
        viewModel.setMes(mes);
    }

    private void createDialog(int position, DespesaListAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Excluir despesa");
        builder.setMessage("");
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Despesa despesa = adapter.getDespesas().get(position);
                viewModel.delete(despesa.getId());
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();
    }
}
