package com.example.victor.driverhelp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.victor.driverhelp.Model.Despesa;
import com.example.victor.driverhelp.R;

import java.text.NumberFormat;
import java.util.List;

public class DespesaListAdapter extends RecyclerView.Adapter<DespesaListAdapter.DespesaListViewHolder> {

    private NumberFormat currrencyFormat;
    private final LayoutInflater layoutInflater;
    private List<Despesa> despesas;

    class DespesaListViewHolder extends RecyclerView.ViewHolder{
        private final TextView txtValor;
        private final TextView txtCategoria;
        private final TextView txtDescricao;
        private final TextView txtData;
        public DespesaListViewHolder(View itemView) {
            super(itemView);
            txtValor = itemView.findViewById(R.id.txtValor);
            txtCategoria = itemView.findViewById(R.id.txtCategoria);
            txtDescricao = itemView.findViewById(R.id.txtDescricao);
            txtData = itemView.findViewById(R.id.txtData);
        }
    }

    public DespesaListAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        currrencyFormat = NumberFormat.getCurrencyInstance();
    }

    public List<Despesa> getDespesas() {
        return this.despesas;
    }

    public void setDespesas(List<Despesa> despesas) {
        this.despesas = despesas;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DespesaListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.despesa_item_layout_item, parent, false);
        return new DespesaListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DespesaListViewHolder holder, int position) {
        Despesa current = despesas.get(position);
        holder.txtDescricao.setText(current.getDescricao());
        holder.txtCategoria.setText(current.getCategoria());
        holder.txtValor.setText(currrencyFormat.format(current.getValor()));
        holder.txtData.setText(current.getData());
    }

    @Override
    public int getItemCount() {
        if(despesas!= null){
            return despesas.size();
        }else{
            return 0;
        }
    }
}
