package com.example.victor.driverhelp.Model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.content.SharedPreferences;
import android.provider.CalendarContract;

import java.util.Calendar;
import java.util.List;

public class DemonstrativoViewModel extends AndroidViewModel {
    private DemonstrativoRepository mRepository;
    private LiveData<List<Demonstrativo>> mDemonstrativos;
    private MutableLiveData<Integer> mes = new MutableLiveData<>();
    private int ano;
    private LiveData<Demonstrativo> demonstrativo;
    private Calendar c;

    public SharedPreferences preferences;

    public DemonstrativoViewModel(Application application) {
        super(application);
        c = Calendar.getInstance();
        mRepository = new DemonstrativoRepository(application);
        //mDemonstrativos = mRepository.getAllDemonstrativos();

        mDemonstrativos = Transformations.switchMap(mes, mes->{
            return mRepository.getDemonstrativos(mes, ano);
        });
        demonstrativo = Transformations.switchMap(mes, mes -> {
            return mRepository.getSumMes(mes, ano);
        });

    }

    public LiveData<List<Demonstrativo>>  getDemonstrativos(){
        return mDemonstrativos;
    }

    public void insert (Demonstrativo demonstrativo){
        mRepository.preferences = this.preferences;
        mRepository.insert(demonstrativo);
    }

    public void delete (Demonstrativo demonstrativo){
        mRepository.delete(demonstrativo);
    }

    public long updateValorLiquido(int demId, double despesa){
        return mRepository.updateValorLiquido(demId, despesa);
    }

    public void setMes(int mes){
        this.mes.setValue(mes);
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public LiveData<Demonstrativo> getDemonstrativo(){
        return demonstrativo;
    }
}
