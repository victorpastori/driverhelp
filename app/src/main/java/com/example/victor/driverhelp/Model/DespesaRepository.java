package com.example.victor.driverhelp.Model;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.victor.driverhelp.DAO.DemonstrativoDAO;
import com.example.victor.driverhelp.DAO.DespesaDAO;
import com.example.victor.driverhelp.DAO.DriverHelpDatabase;

import java.util.List;

public class DespesaRepository {
    private MutableLiveData<Demonstrativo> mDemonstrativo = new MutableLiveData<>();
    private DespesaDAO mDespesaDao;

    private MutableLiveData<Integer> mDemonstrativoId;

    public DespesaRepository(Application application){
        DriverHelpDatabase db = DriverHelpDatabase.getDatabase(application);
        mDespesaDao = db.despesaDAO();
    }

    LiveData<List<DespesaSum>> getSumDespesaMes(int mes, int ano){
        return mDespesaDao.getSumDespesaMes(mes, ano);
    }

    LiveData<List<Despesa>> getmDespesaData(String data){
        return mDespesaDao.getDespesasByData(data);
    }

    LiveData<List<Despesa>> getDespesasMes(int mes, int ano){
        return mDespesaDao.getDespesasMes(mes, ano);
    }

    LiveData<Double> getTotalDespesasMes(int mes, int ano){
        return mDespesaDao.getTotalDespesaMes(mes, ano);
    }

    public LiveData<Demonstrativo> getDemonstrativo(){
        return this.mDemonstrativo;
    }

    public void setDemonstrativo(Demonstrativo demonstrativo){
        this.mDemonstrativo.setValue(demonstrativo);
    }

    public void setDemonstrativoId(int demonstrativoId){
        this.mDemonstrativoId.setValue(demonstrativoId);
    }

    public void delete(int id){
        new deleteAsyncTask(mDespesaDao).execute(id);
    }

    public void insert (Despesa despesa){
        new insertAsyncTask(mDespesaDao).execute(despesa);
    }

    private static class insertAsyncTask extends AsyncTask<Despesa, Void, Void> {

        private DespesaDAO mAsyncTaskDao;
        private DemonstrativoDAO mAsyncTaskDaoDemonstrativo;

        insertAsyncTask(DespesaDAO dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Despesa... despesas) {
            mAsyncTaskDao.insert(despesas[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {

        private DespesaDAO mAsyncTaskDao;
        private DemonstrativoDAO mAsyncTaskDaoDemonstrativo;

        deleteAsyncTask(DespesaDAO dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            mAsyncTaskDao.delete(integers[0].intValue());
            return null;
        }
    }
}
