package com.example.victor.driverhelp.DAO;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.victor.driverhelp.Model.Demonstrativo;
import com.example.victor.driverhelp.Model.Despesa;

@Database(entities = {Demonstrativo.class, Despesa.class}, version = 1)
public abstract class DriverHelpDatabase extends RoomDatabase {

    public abstract DemonstrativoDAO demonstrativoDAO();
    public abstract DespesaDAO despesaDAO();

    private static DriverHelpDatabase INSTANCE;

    public static DriverHelpDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (DriverHelpDatabase.class){
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DriverHelpDatabase.class, "uber_help_db").build();
                }
            }
        }
        return INSTANCE;
    }
}
