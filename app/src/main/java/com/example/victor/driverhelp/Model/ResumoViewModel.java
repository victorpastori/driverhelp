package com.example.victor.driverhelp.Model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import java.util.List;

public class ResumoViewModel extends AndroidViewModel {
    private DespesaRepository despesaRepository;
    private DemonstrativoRepository demonstrativoRepository;
    private LiveData<List<DespesaSum>> despesas;
    private LiveData<Demonstrativo> demonstrativo;

    private MutableLiveData<Integer> mes = new MutableLiveData<>();

    private int ano;

    public ResumoViewModel(@NonNull Application application) {
        super(application);
        despesaRepository = new DespesaRepository(application);
        demonstrativoRepository = new DemonstrativoRepository(application);
        despesas = Transformations.switchMap(mes, mes ->{
            return despesaRepository.getSumDespesaMes(mes, ano);
        });
        demonstrativo = Transformations.switchMap(mes, mes -> {
            return demonstrativoRepository.getSumMes(mes, ano);
        });
    }

    public void setMes(int mes) {
        this.mes.setValue(mes);
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public LiveData<List<DespesaSum>> getDespesas() {
        return despesas;
    }

    public LiveData<Demonstrativo> getDemonstrativo(){
        return demonstrativo;
    }
}
