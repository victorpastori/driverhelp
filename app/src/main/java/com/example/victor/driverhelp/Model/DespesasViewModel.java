package com.example.victor.driverhelp.Model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import java.util.List;

public class DespesasViewModel extends AndroidViewModel {
    private DespesaRepository repository;
    private LiveData<List<Despesa>> despesas;
    private LiveData<Double> despesaTotal;
    private MutableLiveData<Integer> mes = new MutableLiveData<>();
    private int ano;

    public DespesasViewModel(Application application) {
        super(application);
        repository = new DespesaRepository(application);
        despesas = Transformations.switchMap(mes, mes -> {
            return repository.getDespesasMes(mes, ano);
        });

        despesaTotal = Transformations.switchMap(mes, mes -> {
            return repository.getTotalDespesasMes(mes, ano);
        });
    }

    public void delete(int id){
        repository.delete(id);
    }

    public LiveData<List<Despesa>> getDespesas() {
        return despesas;
    }

    public LiveData<Double> getDespesaTotal() {
        return despesaTotal;
    }

    public void setMes(int mes) {
        this.mes.setValue(mes);
    }

    public void setAno(int ano) {
        this.ano = ano;
    }
}
